from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from .views import RegisterView, ChangePasswordView, UpdateProfileView, LogoutView, ProfileView


app_name = 'auth_api'

urlpatterns = [
    path('register/', RegisterView.as_view(), name='auth_register'),
    path('login/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('logout/', LogoutView.as_view(), name='auth_logout'),
    path('change_password/<int:pk>/', ChangePasswordView.as_view(), name='auth_change_password'),
    path('update_profile/<int:pk>/', UpdateProfileView.as_view(), name='auth_update_profile'),
    path('profile/<int:pk>/', ProfileView.as_view(), name='profile')

]