# Lanars test task

**Deployment and launch instructions**

In an empty directory, execute the command::

`git clone https://gitlab.com/artem-svistelnik/lanars_test_task.git`

Create virtual environment (venv) and run it

`python3 -m venv venv`

`source venv/bin/activate`

Go to the directory with the project

`cd lanars_portfolio_project`

Create a postgres db (Replace with your own parameters in the .env file)

Install all required dependencies

`pip install -r requirements.txt `

Apply migration

`python manage.py migrate`

Create superuser

`python manage.py createsuperuser`

run server

`python manage.py runserver `


**Description of requests :**

See swagger

http://localhost:8000/swagger/ 

See redoc

http://localhost:8000/redoc/ 