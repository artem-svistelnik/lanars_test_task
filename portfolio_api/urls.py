from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ImageViewSet, CommentView, PortfolioViewSet

router = DefaultRouter()
router.register(r'images', ImageViewSet)
router.register(r'portfolios', PortfolioViewSet)
app_name = 'portfolio_api'

urlpatterns = [
    path('', include(router.urls)),
    path('comment/', CommentView.as_view(), name='image_comment_create'),
]
