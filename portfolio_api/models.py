from django.contrib.auth.models import User
from django.db import models


class Portfolio(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='portfolio')
    name = models.CharField(max_length=256)
    description = models.TextField()

    def __str__(self):
        return self.name


def save_image(instance, filename):
    return f'portfolio_images/{instance.portfolio.id}/{filename}'


class Image(models.Model):
    portfolio = models.ForeignKey(Portfolio, on_delete=models.CASCADE, related_name='images')
    name = models.CharField(max_length=256)
    description = models.TextField()
    image_url = models.ImageField(upload_to=save_image)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-created_at',)


class Comment(models.Model):
    image = models.ForeignKey(Image, on_delete=models.CASCADE, related_name='comments')
    text = models.TextField(max_length=256)

    def __str__(self):
        return f'Comment #{self.id}'
