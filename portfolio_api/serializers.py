from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import SerializerMethodField

from .models import Comment
from .models import Image
from .models import Portfolio


class PortfolioListSerializer(ModelSerializer):
    class Meta:
        model = Portfolio
        fields = ('id', 'name', 'description')


class PortfolioSerializer(ModelSerializer):
    images = SerializerMethodField('get_portfolio_images')

    class Meta:
        model = Portfolio
        fields = ('id', 'name', 'description', 'images')

    def get_portfolio_images(self, instance: Portfolio):
        return ImageListSerializer(instance.images.all(), many=True).data


class ImageListSerializer(ModelSerializer):
    portfolio_name = SerializerMethodField('get_portfolio_name')

    class Meta:
        model = Image
        fields = ('id', 'name', 'description', 'image_url', 'created_at', 'portfolio_name')

    def get_portfolio_name(self, instance: Image):
        return instance.portfolio.name


class ImageSerializer(ModelSerializer):
    comments = SerializerMethodField('get_comments')

    class Meta:
        model = Image
        fields = ('id', 'portfolio', 'name', 'image_url', 'description', 'comments')

    def get_comments(self, instance: Image):
        return CommentSerializer(instance.comments.all(), many=True).data

    def validate_portfolio(self, portfolio):
        user_portfolios = Portfolio.objects.filter(user=self.context['request'].user).values_list('pk', flat=True)
        if portfolio.id in list(user_portfolios):
            return portfolio
        raise ValidationError('Image creation request rejected. This user does not have this portfolio')


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id','text','image')
