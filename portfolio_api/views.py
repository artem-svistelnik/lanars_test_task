from rest_framework import filters
from rest_framework.generics import CreateAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from .models import Comment
from .models import Image
from .models import Portfolio
from .permissions import IsImageOwner
from .permissions import IsPortfolioOwner
from .serializers import CommentSerializer
from .serializers import ImageListSerializer
from .serializers import ImageSerializer
from .serializers import PortfolioListSerializer
from .serializers import PortfolioSerializer


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 100


class ImageViewSet(ModelViewSet):
    queryset = Image.objects.all()
    filter_backends = [filters.SearchFilter]
    pagination_class = StandardResultsSetPagination
    search_fields = ['name', 'description', 'portfolio__name']

    def get_serializer_class(self):
        if self.action == 'list':
            return ImageListSerializer
        return ImageSerializer

    def get_permissions(self):
        if self.action in ['update', 'partial_update', 'destroy']:
            self.permission_classes = [IsAuthenticated, IsImageOwner]
        elif self.action == 'create':
            self.permission_classes = [IsAuthenticated]
        else:
            self.permission_classes = [AllowAny]
        return super(self.__class__, self).get_permissions()


class CommentView(CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class PortfolioViewSet(ModelViewSet):
    queryset = Portfolio.objects.all()

    def get_permissions(self):
        if self.action in ['update', 'partial_update', 'destroy']:
            self.permission_classes = [IsAuthenticated, IsPortfolioOwner]
        else:
            self.permission_classes = [IsAuthenticated]
        return super(self.__class__, self).get_permissions()

    def get_serializer_class(self):
        if self.action == 'list':
            return PortfolioListSerializer
        return PortfolioSerializer

    def get_queryset(self):
        return Portfolio.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
